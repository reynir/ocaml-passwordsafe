type ctx
(** Some context? *)
(* XXX: Is this stateful?? *)
val key_size : int
(** [key_size] is the size of twofish keys. *)
val init : string -> ctx
(** [init key] is the context / state initialized from secret key [key]. *)
val encrypt : ctx -> string -> string
(** [encrypt ctx plain_text] is a single block encrypted with [ctx]. *)
val decrypt : ctx -> string -> string
(** [encrypt ctx cipher_text] is a single block decrypted with [ctx]. *)
