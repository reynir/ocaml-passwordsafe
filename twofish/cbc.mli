type state =
    {
      mutable prev_v: string;
    }

val init : string -> state
(** [init iv] is the initial state using [iv] a the initialization vector. *)
val encrypt : state -> (string -> string) -> string -> string
(** Encrypt a single block in CBC mode. *)
val decrypt : state -> (string -> string) -> string -> string
(** Decrypt a single block in CBC mode. *)
