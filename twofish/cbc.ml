(* A CBC abstraction layer
   Copyright (C) 2008 Michael Bacarella <mbac@panix.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)


type state =
  {
    mutable prev_v: string;
  }

let init iv =
  {
    prev_v = iv;
  }

let xor_strings b c =
  let int c = int_of_char c in
  String.init (String.length b)
    (fun i ->
       char_of_int @@
       int b.[i] lxor int c.[i])
    
    
let encrypt cbc enc p =
  assert (String.length p = Primitive.key_ize);
  let p' = xor_strings p cbc.prev_v in
  let c = enc p' in
    (cbc.prev_v <- c; c)
      
let decrypt cbc dec c =
  assert (String.length p = Primitive.key_ize);
  let p = dec c in
  let p' = xor_strings p cbc.prev_v in
    (cbc.prev_v <- c; p')
