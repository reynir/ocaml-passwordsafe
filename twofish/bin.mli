
val and32 : Int32.t -> Int32.t -> Int32.t
val xor32 : Int32.t -> Int32.t -> Int32.t
val or32 : Int32.t -> Int32.t -> Int32.t
val right32 : Int32.t -> int -> Int32.t
val left32 : Int32.t -> int -> Int32.t
val add32 : Int32.t -> Int32.t -> Int32.t

val add64 : Int64.t -> Int64.t -> Int64.t

val int64eq : Int64.t -> Int64.t -> bool
val int64true : Int64.t -> bool

val xor4_32 : Int32.t -> Int32.t -> Int32.t -> Int32.t -> Int32.t
